import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from ..models import Todo
from ..serializers import TodoSerializer


# initialize the APIClient app
client = Client()

class GetAllTasksTest(TestCase):

    def setUp(self):
        self.testTodo1 = Todo.objects.create(
            title="Test todo", description="Test description", time="2020-12-23T11:32:00.000Z")
        self.testTodo2 = Todo.objects.create(
            title="Test todo2", description="Test description2", time="2020-12-24T11:32:00.000Z")

        self.valid_payload = { 'title': 'Test createtodo', 'description': 'Test createdescription', 'time': '2020-12-24T11:32:00.000Z' }
        self.invalid_payload = { 'title': '', 'description': 'Test createdescription', 'time': '2020-12-24T11:32:00.000Z' }

    # Test for getting all elements or get one element from the todo table
    def test_get_all_todos(self):
        response = client.get(reverse('get_post_todos'), format="json")
        todos = Todo.objects.all()
        serializer = TodoSerializer(todos, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    # Test creating valid and invalid elements
    def test_create_valid_todo(self):
        response = client.post(
            reverse('get_post_todos'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_todo(self):
        response = client.post(
            reverse('get_post_todos'),
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    # Test for trying to modify an element with valid and invalid data
    def test_update_valid_todo(self):
        response = client.put(
            reverse('update_delete_todos', kwargs={'todo_id': self.testTodo1.pk}),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_invalid_todo(self):
        response = client.put(
            reverse('update_delete_todos', kwargs={'todo_id': self.testTodo1.pk}),
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    # Test for deleting an element with valid ID and invalid ID provided as string
    def test_valid_delete_todo(self):
        response = client.delete(
            reverse('update_delete_todos', kwargs={'todo_id': self.testTodo2.pk}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invalid_delete_todo(self):
        response = client.delete(
            reverse('update_delete_todos', kwargs={'todo_id': 0}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
    