from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
# from django.core.exceptions import ObjectDoesNotExist
from .serializers import TodoSerializer
from .models import Todo

# Create your views here.
def index(request):
    return render(request, 'index.html')

@api_view(["GET", "POST"])
def get_todos(request):
    if request.method == "GET":
        todos = Todo.objects.order_by('id')
        serializer = TodoSerializer(todos, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    elif request.method == "POST":
        serializer = TodoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(["PUT", "DELETE"]) 
def update_todos(request, todo_id):
    try:
        todo_item = Todo.objects.get(id=todo_id)
    except Todo.DoesNotExist:
        return Response({'message': 'The todo does not exist'}, status=status.HTTP_404_NOT_FOUND)
 
    # PUT / DELETE todo
    if request.method == "PUT":
        todo = TodoSerializer(todo_item, data=request.data)
        if todo.is_valid():
            todo.save()
            return Response(todo.data, status=status.HTTP_200_OK)
        return Response(todo.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == "DELETE":
        todo_item.delete()
        return Response({'message': 'Todo was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)