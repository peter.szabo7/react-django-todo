from django.db import migrations

def up(apps, schema_editor):
    # We get the model from the versioned app registry;
    # if we directly import it, it'll be the wrong version
    db_alias = schema_editor.connection.alias
    Todo = apps.get_model("todo", "Todo")

    Todo.objects.using(db_alias).bulk_create([
        Todo(title="Test todo", description="Test description", time="2020-12-23T11:32:00.000Z"),
    ])


def down(apps, schema_editor):
    # up() creates two Country instances,
    # so down() should delete them.
    Todo = apps.get_model("todo", "Todo")

    Todo.objects.all().delete()

class Migration(migrations.Migration):

    dependencies = [
        ('todo', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(up, down),
    ]