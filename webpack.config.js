const path = require("path");
const webpack = require("webpack");
var SRC = path.resolve(__dirname, '/resources/js/');

module.exports = {
  entry: './resources/js/main.js',
  output: {
    path: path.resolve(__dirname, "./static"),
    filename: "[name].js",
  },
  module: {
    rules: [
      {
        test: /\.s(a|c)ss$/,
        use: [
          // Creates `style` nodes from JS strings
          "style-loader",
          // Translates CSS into CommonJS
          "css-loader",
          // Compiles Sass to CSS
          "sass-loader",
        ]
      },
      {
        test: /\.(jpg|jpeg|gif|png|svg)$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            publicPath: 'images',
            outputPath: 'images',
          }
        }
      },

      {
        test: /\.(eot|ttf|woff|woff2)$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            publicPath: 'fonts',
            outputPath: 'fonts',
          }
        }
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
    ],
  },
  optimization: {
    minimize: true,
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        // This has effect on the react lib size
        NODE_ENV: JSON.stringify("production"),
      },
    }),
  ],
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: ['node_modules', path.resolve(__dirname, 'core')],
    fallback: {
      fs: false,
      child_process: false,
      module: false,
      util: false,
      path: false,
      crypto: false,
      buffer: false,
      https: false,
      url: false,
      http: false,
      vm: false,
      querystring: false,
      os: false,
      stream: false,
      constants: false,
      assert: false
    }
  },
};