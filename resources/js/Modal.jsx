import React, { useEffect, useState } from 'react';
import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter, MDBInput } from 'mdbreact';

function Modal(props) {
    // const [isCreate, setIsCreate] = useState(true)
    const [title, setTitle] = useState("")
    const [description, setDescription] = useState("")
    const [time, setTime] = useState("")

    const handleTitle = e => setTitle(e.target.value);
    const handleDescription = e => setDescription(e.target.value);
    const handleTime = e => setTime(e.target.value);

    // function toggle () {
    //     props.onCloseModal()
    // }

    useEffect(() => {
        setTitle(props.taskToEdit.title)
        setDescription(props.taskToEdit.description)
        setTime(props.taskToEdit.time)

    }, [props.taskToEdit]);

    function saveCreateTask (mode) {
        props.onSaveCreateTask({
            title: title,
            description: description,
            time: time
        }, mode)
        props.onCloseModal()
    }
    
    return (
        <MDBModal isOpen={props.show} toggle={() => props.onCloseModal()}>
            <MDBModalHeader color={props.isCreate ? "success" : "primary"}>{props.isCreate ? "Create New Task" : "Edit Task"}</MDBModalHeader>
            <MDBModalBody>
                <MDBInput label="Name of Task*" className="mt-5" maxlength="200" value={title} onChange={handleTitle}/>
                <MDBInput label="Description" type="textarea" rows="3" value={description} className="mt-5" maxlength="200" onChange={handleDescription}/>
                <MDBInput label="Time*" className="mt-5" maxlength="10" value={time} onChange={handleTime}/>
            </MDBModalBody>
            <MDBModalFooter>
                {props.isCreate &&
                    <MDBBtn color="success" onClick={() => saveCreateTask('create')}>Create</MDBBtn>
                }
                {!props.isCreate &&
                    <MDBBtn color="info" onClick={() => saveCreateTask('save')}>Save</MDBBtn>
                }
                {!props.isCreate &&
                    <MDBBtn color="danger" onClick={() => props.onDeleteTask()}>Delete</MDBBtn>
                }
                
                <MDBBtn color="secondary" onClick={() => props.onCloseModal()}>Cancel</MDBBtn>
            </MDBModalFooter>
        </MDBModal>
    );
    
}

export default Modal;