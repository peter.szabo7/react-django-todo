import React, { useEffect, useState } from 'react'
import Axios from 'axios'
import { ProgressPlugin } from 'webpack'
import { MDBBtn, MDBCard, MDBCardBody, MDBCardTitle, MDBCardText, MDBCardHeader } from 'mdbreact';
import moment from 'moment'
import _ from 'lodash';
import Modal from './Modal'

const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;
window.axios = Axios;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers.common['X-CSRFToken'] = csrftoken;

function App() {
    const [isCreate, setIsCreate] = useState(true)
    const nowday = moment().format("dddd, Do")
    const nowmonth = moment().format("MMMM")
    const [todos, setTodos] = useState([])
    const [modalVisible, setModalVisible] = useState(false)
    const [taskToEdit, setTaskToEdit] = useState({})

    useEffect(() => {
        axios.get("/api/todos").then(response => {
            setTodos(response.data);
        });
    }, []);

    function timeFromat (time) {
        return moment(time).format("HH:mm")
    }

    function changeCheckbox (event) {
        let tmpTodos = [...todos]
        let theTodo = _.find(tmpTodos, (todo) => {return todo.id == parseFloat(event.target.name)})
        theTodo.done = event.target.checked
        setTodos(tmpTodos)
    }

    function showHideModal (task) {
        setModalVisible(!modalVisible)

        if (task) {
            setIsCreate(false)
            setTaskToEdit(task)
        } else {
            setIsCreate(true)
            setTaskToEdit({
                time: "",
                description: "",
                title: ""
            })
        }
    }

    function saveCreateTask (task, mode) {
        let tmpTodos = [...todos]


        if (mode === "create") {
            axios.post("api/todos", task).then(resp => {
                tmpTodos.push(resp.data)
                setTodos(tmpTodos)
            }).catch(er => {
                alert("Sorry, something went wrong!")
                console.log(er)
            })
        } else {
            axios.put("api/updatetodo/" + taskToEdit.id, task).then(resp => {
                let modifiedTask = _.find(tmpTodos, (todo) => {return todo.id === taskToEdit.id})
                for (let prop in modifiedTask) {
                    modifiedTask[prop] = resp.data[prop]
                }
                setTodos(tmpTodos)
            }).catch(er => {
                alert("Sorry, something went wrong!")
                console.log(er)
            })
        }
    }

    function deleteTask () {
        let tmpTodos = [...todos]

        if (confirm("Are you sure you want to delete this task?")) {
            axios.delete("api/updatetodo/" + taskToEdit.id).then(() => {
                setModalVisible(false)
                _.remove(tmpTodos, (todo) => {return todo.id === taskToEdit.id})
                setTodos(tmpTodos)
            }).catch(er => {
                alert("Sorry, something went wrong!")
                console.log(er)
            })
        } 
    }

    return (
        <div class="col-lg-6 col-xl-4 col-md-6 mx-auto my-5">
            <MDBCard>
                <MDBCardHeader className="position-relative">
                    <div class="row mt-4">
                        <div class="col-8">
                            <h2 class="text-primary m-0">{nowday}</h2>
                        </div>
                        <div class="col-4 d-flex align-items-end justify-content-end">
                            <span class="text-black-50">{todos.length} Tasks</span>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-8">
                            <p class="text-black-50">{nowmonth}</p>
                        </div>
                    </div>
                    <MDBBtn color="danger" onClick={() => showHideModal()} className="roundbutton">+</MDBBtn>
                </MDBCardHeader>
                <MDBCardBody className="p-0">
                    <div class="mt-5">
                        {todos.length > 0 && todos.map(todo => (
                            <div key={todo.id} class="row mx-0 taskrow border-bottom border-light">
                                <div className="col-auto d-flex align-items-center justify-content-center py-3">
                                    <input color="danger" name={todo.id} type="checkbox" onChange={changeCheckbox}/>
                                </div>
                                <div onClick={() => showHideModal(todo)} className={`col-8 col-lg-8 d-flex align-items-center py-3 ${todo.done ? 'inactive inactivecolor' : ''}`}>
                                    <span>{todo.title}</span>
                                </div>
                                <div onClick={() => showHideModal(todo)} className={`col-auto d-flex align-items-center py-3 ${todo.done ? 'inactivecolor' : ''}`}>
                                    <span>{timeFromat(todo.time)}</span>
                                </div>
                            </div>
                        ))}
                        
                        {todos.length === 0 && 
                        <div class="row px-4">
                            <div class="col">
                                <p class="p emptylist text-black-50">Add a task with the plus button...</p>
                            </div>
                        </div>
                        }
                        
                    </div>
                </MDBCardBody>
            </MDBCard>
            <Modal onCloseModal={showHideModal} 
                onSaveCreateTask={saveCreateTask} 
                onDeleteTask={deleteTask}
                show={modalVisible} 
                taskToEdit={taskToEdit} 
                isCreate={isCreate}
            />
        </div>
    )  
    
}

export default App
